#!/bin/bash

DEV=/dev/ttyACM0

LOGNAME=tmp_log
if [ "$#" = "1" ]; then
    LOGNAME="$1"
fi

TOOL="sudo Software/chamtool.py -p $DEV"

$TOOL --log $LOGNAME.bin

hexdump -C $LOGNAME.bin > $LOGNAME.hex
cat $LOGNAME.hex

Software/chamlog.py -f $LOGNAME.bin -t json > $LOGNAME.json
Software/chamlog.py -f $LOGNAME.bin > $LOGNAME.txt
cat $LOGNAME.txt

