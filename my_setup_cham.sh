#!/bin/bash

DEV=/dev/ttyACM0

TOOL="sudo Software/chamtool.py -p $DEV"

$TOOL -i

$TOOL --setting 1
$TOOL --config MF_ULTRALIGHT_C
$TOOL --upload /home/bartezza/Dropbox/Hax/RFID/Dumps/dixBillDemi-orig.bin
$TOOL --uid
$TOOL --rbutton CYCLE_SETTINGS
$TOOL --store

$TOOL --setting 2
$TOOL --config ISO14443A_SNIFF
$TOOL --logmode MEMORY

$TOOL --gled MEMORY_STORED
$TOOL --rled SETTING_CHANGE

$TOOL --rbutton CYCLE_SETTINGS
$TOOL --lbutton STORE_LOG
$TOOL --store

